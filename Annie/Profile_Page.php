<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User Profile</title>
    <style>

    body {
    	margin: 0;
    	padding: 0;
    	background: #590D17;
    	font-family: 'Arial', serif;
    	font-size: 14px;
    	color: #FFFFFF;
    }

    h1, h2, h3 {
    	margin: 0px;
    	padding: 0px;
    	font-weight: 400;
    }

    #header {
    	overflow: hidden;
    	width: 900px;
    	margin: 0 auto;
    }

    #logo h1 {
    	display: block;
    	letter-spacing: -3px;
    	text-align: center;
    	font-size: 64px;
    }

    #page {
    	width: 900px;
    	margin: 0px auto;
    }

    #content {
    	float: left;
    	width: 540px;
    }

    .post {
    	margin-bottom: 30px;
    	padding: 40px 40px;
    	background: rgba(0,0,0,.5);
    }

    .post .title {
      text-align:center;
    	color: #FFFFFF;
    }

    #sidebar {
    	float: right;
    	width: 290px;
    	padding: 40px 20px;
    	background: rgba(0,0,0,.5);
    }

    #sidebar ul {
    	margin: 0;
    	padding: 0;
    	list-style: none;
    }

    #sidebar li {
    	margin: 0;
    	padding: 0;
    }

    #sidebar li ul {
    	margin: 0px 15px;
    	padding-bottom: 30px;
    }

    #sidebar li li {
    	line-height: 35px;
    }

    #sidebar h2 {
    	height: 38px;
    	padding: 12px 0 20px 15px;
    	letter-spacing: -1px;
    	color: #FFFFFF;
      text-align:center;
    }

    #about {
    	overflow: hidden;
    	width: 820px;
    	margin: 30px auto 30px auto;
    	padding: 40px 40px;
    	background: rgba(0,0,0,.5);
    }

    #about .title {
    	padding-bottom: 30px;
    	letter-spacing: -1px;
    	color: #FFFFFF;
      text-align:center;
    }

    .image{
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
    }

    .imageee{
      display: block;
      margin-left: auto;
      margin-right: auto;
      width: 40%
    }

    .connections{
      text-align:center;
    }
    .button{
      position: absolute;
      top: 10px;
      right: 10px;
    }
    </style>
  </head>

  <body>

        <button><a href="Profile_Edit_Page.php">Edit Profile</a></button>

    	<div id="header">
    		<div id="logo">
    			<h1>Annie Medding's Profile</h1>
    		</div>
    	</div>

    	<div id="about">
    		<h2 class="title">Profile Overview</h2>
    		<p><img src="profileavatar.jpg" width="200" height="200" class="image" />
          <br />
          <p> Annie Medding is currently a Business Information Technology Major with a focus in Decision Based Support Systems with a minor in Strategic Commnuications.
          She is currently a senior at Virginia Tech and is looking forward to graduating in Spring 2020.</p>
        </p>
    	</div>

    	<div id="page">
    		<div id="content">

    			<div class="post">
    				<h2 class="title">About Me</h2>
    					<p> Annie Medding is currently a Business Information Technology Major with a focus in Decision Based Support Systems with a minor in Strategic Commnuications.
              She is currently a senior at Virginia Tech and is looking forward to graduating in Spring 2020.</p>
    			</div>

    			<div class="post">
    				<h2 class="title">Recent Activity</h2>
    				<div class="entry">
              <ul>
    						<li>Annie Medding updated their profile picture</li>
                <br />
    						<li>Annie Medding created a new business page</li>
                <br />
    						<li>Annie Medding commented in a forum</li>
                <br />
    						<li>Annie Medding has created a new connection with Julie Crowe</li>
                <br />
    						<li>Annie Medding has updated their "About Me" section</li>
                <br />
    						<li>Annie Medding commented in a forum</li>
    					</ul>
    				</div>
    			</div>
    		</div>
    		<div id="sidebar">
    			<ul>
    				<li>
    					<h2>Connections</h2>
    					<ul>
    						<li><img src="profileavatar.jpg" width="100" height="100" class="imageee" /></li>
    						<li class="connections"> Julie Crowe</li>
                <li><img src="profileavatar.jpg" width="100" height="100" class="imageee" /></li>
                <li class="connections"> George Scharps</li>
                <li><img src="profileavatar.jpg" width="100" height="100" class="imageee" /></li>
                <li class="connections"> Joseph Spence</li>
                <li><img src="profileavatar.jpg" width="100" height="100" class="imageee" /></li>
                <li class="connections"> Ryan Daniels</li>
    					</ul>
    				</li>
    		</div>

    	</div>
    </div>


  </body>
</html>
