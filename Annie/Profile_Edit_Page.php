<!DOCTYPE html>
<html>
<head>
  <style>
  .heading {
    text-align: center;
    color: #800000;
    font-family: Arial;
  }
  </style>
</head>

<body>

<div class="container">
    <h1 class="heading">Edit Profile</h1>
  	<hr>
	<div class="row">
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h4>Upload a different photo...</h4>

          <input type="file" class="form-control">
        </div>
      </div>

      <div class="col-md-9 personal-info">

        <h3>Personal info</h3>

        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-lg-3 control-label">First name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="Jane">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Last name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="Bishop">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Major:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="BIT">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Courses Taken:</label>
            <div class="col-lg-8">
              <textarea rows="4" cols="50" name="comment" form="usrform">
                Enter text here...</textarea>
            </div>
          </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">About Me:</label>
            <div class="col-md-8">
              <textarea rows="4" cols="50" name="comment" form="usrform">
                Enter text here...</textarea>
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<hr>
</body>
</html>
