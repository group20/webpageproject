<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Course Catalog</title>

    <style>
    table.blueTable {
      border: 1px solid #590D17;
      background-color: #EEEEEE;
      width: 100%;
      text-align: left;
      border-collapse: collapse;
    }
    table.blueTable td, table.blueTable th {
      border: 1px solid #AAAAAA;
      padding: 3px 2px;
    }
    table.blueTable tbody td {
      font-size: 13px;
    }
    table.blueTable tr:nth-child(even) {
      background: #ffc299;
    }
    table.blueTable thead {
      background: #590D17;
      background: -moz-linear-gradient(top, #590D17 0%, #590D17 66%, #590D17 100%);
      background: -webkit-linear-gradient(top, #590D17 0%, #590D17 66%, #590D17 100%);
      background: linear-gradient(to bottom, #590D17 0%, #590D17 66%, #590D17 100%);
      border-bottom: 2px solid #444444;
    }
    table.blueTable thead th {
      font-size: 15px;
      font-weight: bold;
      color: #FFFFFF;
      border-left: 2px solid #ffc299;
    }
    table.blueTable thead th:first-child {
      border-left: none;
    }

    table.blueTable tfoot {
      font-size: 14px;
      font-weight: bold;
      color: #FFFFFF;
      background: #ffc299;
      background: -moz-linear-gradient(top, #ffc299 0%, #ffc299 66%, #ffc299 100%);
      background: -webkit-linear-gradient(top, #ffc299 0%, #ffc299 66%, #ffc299 100%);
      background: linear-gradient(to bottom, #590D17 0%, #590D17 66%, #590D17 100%);
      border-top: 2px solid #444444;
    }
    table.blueTable tfoot td {
      font-size: 14px;
    }
    table.blueTable tfoot .links {
      text-align: right;
    }
    table.blueTable tfoot .links a{
      display: inline-block;
      background: #590D17;
      color: #FFFFFF;
      padding: 2px 8px;
      border-radius: 5px;
    }

    .heading{
      text-align: center;
      font-family: Arial;
      color: #590D17
    }
    .button{
      position: absolute;
      top: 10px;
      right: 10px;
    }
    </style>

  </head>
  <body>
    <button><a href="../GeorgesPages/addPage.php">Add A Course</a></button>


    <h1 class="heading"> Course Catalog</h1>
    <h5 class="heading"> Welcome to the Virginia Tech course catalog! This is a place where you can browse the various courses that are offered at Virginia Tech</h5>

    <?php
    // populating the table with php from the database
      require_once ("db.php");
//
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Introduction to BIT', 'Learn how to use finance in a practical setting', '1111', 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Introduction to Art', 'Learn how to use art in a practical setting', '1111', 36, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Introduction to Music', 'Learn how to use music in a practical setting', '1111', 37, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Introduction to Design', 'Learn how to use design in a practical setting', '1111', 38, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Marketing', 'Learn how to use marketing in a practical setting', '1111', 39, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Systems Development', 'Learn how to use systems development in a practical setting', '1111', 40, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Database Design', 'Learn how to use database design in a practical setting', '1111', 41, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Calculus', 'Learn how to use calculus in a practical setting', '1111', 42, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum, Major_ID) VALUES ('Strategic Management', 'Learn how to use strategic Management in a practical setting', '1111', 43, 3)";
      //   $result = $mydb->query($sql);
      // $sql = "INSERT INTO Classes (ClassName, Description, ClassNum,Major_ID) VALUES ('Project Management', 'Learn how to use project management in a practical setting', '1111', 44, 3)";
      // $result = $mydb->query($sql);

      $sql = "SELECT ClassName, Description, ClassNum FROM classes";
      $result = $mydb->query($sql);
      echo "<table class='blueTable';>
      <thead>
        <th>
          Class Name
        </th>
        <th>
          Description
        </th>
        <th>
          CRN
        </th>
      </thead>
      <tbody>";
      while($row = mysqli_fetch_array($result)){
        echo "<tr>";
        echo "<td>".$row['ClassName'];
        echo "<td>".$row['Description'];
        echo "<td>".$row['ClassNum'];
        echo "</tr>";
      }
      echo "</tbody>
      </table>";

    ?>


       </table>
  </body>
</html>
