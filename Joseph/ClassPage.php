<?php 
     function sendInfo(){
         $class = "BIT 4444";
         $url = "General_Edit_Page.php?class=".$class;
        header("Location:".$url);
        exit();

    }
    function goHere(){
        $class = "Web-Based DSS";
         $url = "../Julie/ClassForum.php?class=".$class;
        header("Location:".$url);
        exit();
    }

?>
<!DOCTYPE html>
<Html>
<Head>
    <Title>Class Page</Title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="MyCss.css">
    <style>
        #HeaderClass {
            background-color: maroon;
            text-align: center;
        }
        #HeaderClass h1 {
            color: White;
        }
        #CourseBody, #CourseBodyHeader, #CourseContentOutline, #Sections {
            border-color: black;
            border-style: solid;
            border-width: 1px;
        }
        #CourseBodyHeader {
            background-color: lightblue;
        }

        #CourseBodyHeader button {
            background-color: maroon;
            color: white;
        }
        body {
            /* height: 3000px; Used to enable scrolling */
            background: linear-gradient(55deg, lightgrey 0%, lightgray 51%, white 85%);
            
        }
    </style>
    <script>

        $(document).ready(function() {
        // Transition effect for navbar 
            toggleNav();
            $(window).scroll(function() {
                // checks if window is scrolled more than 500px, adds/removes solid class
                if($(this).scrollTop() > $(window).height()/2 ) { 
                    $('.navbar').addClass('solid');
                    $('.UserGreeter').addClass('solid');
                } else {
                    $('.navbar').removeClass('solid');
                    $('.UserGreeter').removeClass('solid');
                }
            });
            $(window).resize(function(){
                toggleNav();
            }); 
        });

        function toggleNav(){
            if($(window).width() > 1546) {
                $("#logoPic").show();
                $(".shortlogoPic").hide();
            } else {
                $("#logoPic").hide();
                $(".shortlogoPic").show();
            }
        }

    </script>
</Head>



<Body>
    <!-- Navigation Bar -->
    
    <!-- Leftside Logo -->
    <nav id="logoPic" class="navbar navbar-default navbar-fixed-top">
        <img src="HokiepediaLogo.jpg">
        <div class="UserGreeter">
            <label>Hello, <?php session_start(); 
                                if(empty($_SESSION['name'])) {
                                    echo "User";
                                } else {
                                    echo $_SESSION['name'];
                                }  
                                ?>
            </label>
        </div>
    </nav>

    <!-- Navagation Bar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../GeorgesPages/homePage.php"><i class="glyphicon glyphicon-home"></i> <label>Home</label></a>
                <a class="navbar-brand" href="../Annie/Course_Catalog.php"><i class="glyphicon glyphicon-apple"></i><label>Class</label></a>
                <a class="navbar-brand" href="../Annie/departmentCatalog.php"><i class="glyphicon glyphicon-education"></i><label>Majors</label></a>
                <a class="navbar-brand" href="../Julie/BusinessPage.php"><i class="glyphicon glyphicon-usd"></i><label>Business</label></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../Annie/Profile_Page.php"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                    <li><a href="../Ryan/AccountSettingsPage.php"><i class="glyphicon glyphicon-cog"></i>Settings</a></li>
                    <li><a href="../GeorgesPages/homePage.php"> <i class="glyphicon glyphicon-remove-sign"></i> Logout</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>


    <!-- Class Title -->

    <div class="container">

     <!-- Center Logo when resized -->
     <div class="shortlogoPic">
        <img src="HokiepediaLogo.jpg">
        <div class="UserGreeter">
            <label>Hello, <?php session_start(); 
                                if(empty($_SESSION['name'])) {
                                    echo "User";
                                } else {
                                    echo $_SESSION['name'];
                                }  
                                ?>
            </label>
        </div>
    </div>


    <div class="headerText">
        <h1><?php 
                if(isset($_GET['HeaderName']) && isset($_GET['type'])){
                    $Header = $_GET['HeaderName']; 
                    $type = $_GET['type'];
                    
                    echo $type. " ".$Header;
                }
            ?></h1>
 
    </div>
        

        <!-- Class Dashboard -->
        <div class="container-fluid" id="CourseBody">
            
            <!-- Class header Info -->
            <div class="row" id="CourseBodyHeader">
                <div class="col-md-3">

                </div>
                <div class="col-md-2" id="CBH_Button">
                    <form action="../Julie/ClassForum.php?class=Web-Based DSS"></form>
                        <button type="submit" name="submit" onclick="window.location.href='../Julie/ClassForum.php?class=Web-Based DSS' ">
                        BIT 4444 Forum</button>
                        <!-- Needs to be a button that sends an id to the forum page -->
                        </form>
                </div>
                <div class="col-md-2">
                    <label><a href="">Add Grade</a></label>
                    <br>
                    <label><a href="">Enrolled</a></label>
                    <br>
                    <label><a href="">Completed</a></label>
                </div>
                <div class="col-md-5">
                    <h1>A GRAPH</h1>
                </div>
            </div>
            <br>

        <!-- Class Content Outline -->
            <div id="CourseContentOutline">
                <div class="row">
                    <div class="col-md-12">
                        <label>COURSE CONTENT</label>
                        <label><?php 
                            
                                    $Cl_Mj = "";
                                    $type = "";
                                    $id = 0;
                                    if(isset($_GET['obj']) && isset($_GET['Objtype']) && isset($_GET['ids'])) {
                                        $Cl_Mj=$_GET['obj'];
                                        $type=$_GET['Objtype'];
                                        $id =$_GET['ids'];
                                    } else {
                                        $Cl_Mj = "Web-Based DSS";
                                        $type = "Class";
                                        $id= 4;
                                    }
                                    echo "<a href='General_Edit_Page.php?HeaderName=".$Cl_Mj."&type=".$type."&CID=".$id."'><u>Add Content (+)</u></a>";
                                ?>
                        </label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <ul>    <!-- MySQL CODE HERE TO FILL IN List -->
                        <?php 
                            require_once("db.php");
                            $ClassNum = 0;
                            if(isset($_GET['classNum'])) {
                                $ClassNum=$_GET['classNum'];
                                
                            } else {
                                $ClassNum = 4444;
                                $type = "Class";
                            }

                            $sql = "SELECT  DISTINCT HeaderIndex, ClassName, ClassNum, Contribution, HeaderName
                                    FROM ClassContribution as x, classes as c
                                    WHERE x.Class_ID = c.Class_ID
                                    AND c.ClassNum = $ClassNum
                                    ORDER BY HeaderIndex";
                            $result = $mydb->query($sql);

                            while($row=mysqli_fetch_array($result)){  
                                echo    "<li><a href=''>".$row['HeaderName']."</a></li>";
                                }
                        ?>              
                        </ul>
                    </div>
                </div>
            </div>

        <!-- Class Content -->
            <div class="jumbotron" id="Sections">
            <?php 
                require_once("db.php");
                $ClassNum = 0;
                if(isset($_GET['classNum'])) {
                    $ClassNum=$_GET['classNum'];
                    
                } else {
                    $ClassNum = 4444;
                    $type = "Class";
                }

                $sql = "SELECT  DISTINCT HeaderIndex, ClassName, ClassNum, Contribution, HeaderName
                        FROM ClassContribution as x, classes as c
                        WHERE x.Class_ID = c.Class_ID
                        AND c.ClassNum = $ClassNum
                        ORDER BY HeaderIndex";
                $result = $mydb->query($sql);

                while($row=mysqli_fetch_array($result)){  
                    echo    "<div class='jumbotron jumbotron-fluid'>
                                <div class='container'>
                                    <h1 class='display-4'>".$row['HeaderName']."</h1>
                                    <p class='lead'>".$row['Contribution']."</p>
                                </div>
                            </div>";
                    }
            ?>
            </div>

           
        </div> <!-- End of the Course Body Data -->
    </div> <!-- The end of the container -->
 
    <div class="footer">
        <div class="row">
            <div class="col-lg-4" id="aboutfooter">
                <footer>
                    About Us
                    <br>
                    BIT 4444 Team Production
                </footer>
            </div>
            <div class="col-lg-6">
            </div>
            <div class="col-lg-2" id="aboutfooter2">
                <footer>
                    <u>Hokiepedia inc.</u>
                </footer>
            </div>
        </div>
</Body>


</Html>