<!-- Joseph's Page -->
<?php

    $FirstN = "";
    $LastN = "";
    $RadioGRUG= 0;
    $Email = "";
    $Major = "";
    $Option = "";
    $Username ="";
    $Password = "";
    $err = true;

    if (isset($_POST["submit"])){
        if(isset($_POST["FirstN"]))     $FirstN = $_POST["FirstN"];
        if(isset($_POST['LastN']))      $LastN = $_POST['LastN'];
        if(isset($_POST['TypeSel']))    $RadioGRUG = $_POST['TypeSel'];
        if(isset($_POST['UEmail']))     $Email = $_POST['UEmail'];
        if(isset($_POST['UserN']))      $Username = $_POST['UserN'];
        if (isset($_POST["PWD"]))       $Password = $_POST["PWD"];
        if(isset($_POST['UMajor']))     $Major = $_POST['UMajor'];
        if(isset($_POST['Uopt']))       $Option = $_POST['Uopt'];
        $err = false;
    }

    if(!$err && !empty($FirstN) && !empty($Username) && !empty($Password) && $RadioGRUG !=0){
        require_once("db.php");

        $sql = "INSERT INTO user (UserType_ID, Username,  Email, firstName, lastName, Major, PWD) 
                VALUES ($RadioGRUG, '$username', '$Email', '$FirstN', '$LastN', '$Major', '$Password')";
        $result = $mydb->query($sql);
  
          //set session variable to remember the username
        session_start();
        $_SESSION["username"] = $username;
        $_SESSION["name"] = $FirstN;
        $_SESSION["Major"] = $Major;
        header("HTTP/1.1 307 Temporary Redirect");
        header("Location: ../GeorgesPages/HomePage.php");
    }
?>

<!DOCTYPE html>
<Html>



<Head>
    <Title>Account Creation</Title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="MyCss.css">

    <style>
        #HeaderAcct {
            border-style: solid;
            background-color: maroon;
            color: white;
            align-content: center;
            text-align: center;
            margin-left: 400px;
            margin-right: 400px;
            min-width: 400px;
        }
        .AccountArea {
            background-color: lightsteelblue;
            border-style: solid;
            box-shadow: 4px 4px gray;
        }
        #GenText {
            width: 300px;
            
            
        }
        body {
            min-height: 1100px;
        }

        .row {
            margin-left:45px;
            margin-bottom: 25px;
        }

    </style>
    <script>

        $(document).ready(function() {
        // Transition effect for navbar 
            toggleNav();
            $(window).scroll(function() {
                // checks if window is scrolled more than 500px, adds/removes solid class
                if($(this).scrollTop() > $(window).height()/2 ) { 
                    $('.navbar').addClass('solid');
                    $('.UserGreeter').addClass('solid');
                } else {
                    $('.navbar').removeClass('solid');
                    $('.UserGreeter').removeClass('solid');
                }
            });
            $(window).resize(function(){
                toggleNav();
            }); 
        });

        function toggleNav(){
            if($(window).width() > 1546) {
                $("#logoPic").show();
                $(".shortlogoPic").hide();
            } else {
                $("#logoPic").hide();
                $(".shortlogoPic").show();
            }
        }
    </script>

</Head>
<body> 
   <!-- Leftside Logo -->
<nav id="logoPic" class="navbar navbar-default navbar-fixed-top">
    <img src="HokiepediaLogo.jpg">
    <div class="UserGreeter">
        <label>WELCOME</label>
    </div>
</nav>

<!-- Navagation Bar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../GeorgesPages/homePage.php"><i class="glyphicon glyphicon-home"></i> <label>Home</label></a>
            <a class="navbar-brand" href="../Annie/Course_Catalog.php"><i class="glyphicon glyphicon-apple"></i><label>Class</label></a>
            <a class="navbar-brand" href="../Annie/departmentCatalog.php"><i class="glyphicon glyphicon-education"></i><label>Majors</label></a>
            <a class="navbar-brand" href="../Julie/BusinessPage.php"><i class="glyphicon glyphicon-usd"></i><label>Business</label></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="shortlogoPic">
    <img src="HokiepediaLogo.jpg">
    <div class="UserGreeter">
        <label>WELCOME</label>
    </div>
</div>


    <!-- Account Content -->
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    
        <div class="container">
            <div class="headerText">
                <h1>Account Creation</h1>
            </div>
            
            <div class="AccountArea">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <label>Gradutate or Undergrade?</label>
                        <div class="form-group">
                            <label for="ClassRadio" class="radio-inline">
                            <input type="radio" name="TypeSel" value="2" id="GradRadio"
                            <?php if (isset($_POST['TypeSel']) && $_POST['TypeSel'] == 2): ?> checked='checked' <?php endif; ?> >
                                Graduate
                            </label>
                            <label for="MajorRadio" class="radio-inline">
                                <input type="radio" name="TypeSel" value="1" id="UndergradRadio"
                                <?php if (isset($_POST['TypeSel']) && $_POST['TypeSel'] == 1): ?> checked='checked' <?php endif; ?>  >
                                    Undergrade
                            </label>
                        </div>
                    </div>
                </div>
                <br>


                <div class="row">
                    <div class="col-lg-3">
                        <label>First Name</label>
                    </div>
                    <div class="col-lg-9">
                        <input id="GenText" name="FirstN" type="text"/>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-lg-3">
                        <label>Last Name</label>            
                    </div>
                    <div class="col-lg-9">
                        <input id="GenText" name="LastN" type="text"/>
                    </div>
                </div> 
                <br>

                <div class="row">
                    <div class="col-lg-3">
                        <label>Email</label>
                        <br>
                        <small><sup>***</sup>Must be a vt.edu account<sup>***</sup></small>
                    </div>
                    <div class="col-lg-9">
                        <input id="GenText" name="UEmail" type="text"/>
                    </div>
                </div> 
                <br>


                <div class="row">
                    <div class="col-lg-1"> </div>
                    <div class="col-lg-2">
                        <label>Major</label>                
                    </div>
                    <div class="col-lg-2">
                        <input name="UMajor" type="text"/>
                    </div>
                    <div class="col-lg-1">
                        <label>Options</label>                        
                    </div>
                    <div class="col-lg-2">
                        <input name="UOpt" type="text"/>
                    </div>
                </div> 
                <br>

                <div class="row">
                    <div class="col-lg-3">
                        <label>Username</label>
                    </div>
                    <div class="col-lg-9">
                        <input id="GenText" name="UserN" type="text"/>
                    </div>
                </div>
                <br>
                    
                <div class="row">
                    <div class="col-lg-3">
                        <label>Password</label>
                    </div>
                    <div class="col-lg-9">
                        <input id="GenText" name="PWD" type="text"/>
                    </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                    
                <div class="row">
                    <div class="col-lg-2">
                        <button type="submit" type="submit"  name="submit" class="Endbutton">Create</button>
                    </div>
                    <div class="col-lg-2">
                        <button class="Endbutton"><a href="../GeorgesPages/homePage.php">Cancel</a></button>
                    </div>
                </div>

                <br>          
                <br>

            </div>        
        </div>      
    </form>

    <div class="footer">
        <div class="row">
            <div class="col-lg-4" id="aboutfooter">
                <footer>
                    About Us
                    <br>
                    BIT 4444 Team Production
                </footer>
            </div>
            <div class="col-lg-5">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-2" id="aboutfooter2">
                <footer>
                    <u>Hokiepedia inc.</u>
                </footer>
            </div>
        </div>
    </div>

</Body>

</Html>