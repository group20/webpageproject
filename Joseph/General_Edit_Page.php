<!-- Joseph's Page -->
<!DOCTYPE html>
<?php
    $TextArea ="";
    $classId = "4";
    $HeaderIndex =1;
    $textError="False";

    if(isset($_POST['submit'])) {
        if(isset($_POST['data']))   $TextArea=$_POST['data'];
        if(isset($_GET['CID']))     $classId=$_GET['CID'];
        if(isset($_POST['HI']))     $HeaderIndex=$_POST['HI'];

        require_once("db.php");
            // INSERT INTO ClassContribution (Class_ID, HeaderIndex, HeaderName, Contribution) VALUES ($classiId, $HeaderIndex, 'Typcial Syllabus','$TextArea')";
        $sql = "UPDATE `ClassContribution` SET `Contribution`='$TextArea' WHERE Class_ID = $classId AND HeaderIndex = $HeaderIndex";
        $result = $mydb->query($sql);

        header("HTTP/1.1 307 Temporary Redirect");
      header("Location: ClassPage.php");
   
    } else {
        $textError = true; 
    }
?>


<Html>
<Head>
    <Title>EDIT CLASS</Title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="MyCss.css">
    <style>
        
        #HeaderClass {
            background-color: maroon;
            color: white;
            text-align: center;
        }
        #TextChangeHeader{
            text-align: right;
            margin-bottom: 100px;
        }
        .headerText {
            margin-bottom: 100px;
        }
        #ButtonDivs {
            margin-top: 50px;
        }

    </style>
    <script src="https://cdn.tiny.cloud/1/4of2n961lvuhmwbg31ghm8k2dzl1949uwvq773gpa2an43x1/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({ 
            selector:'textarea', 
            plugins: 'link',
            menubar: false,
            toolbar: 'undo redo | selectall copy paste cut | bold italic underline | alignleft aligncenter alignright alignnone | link '
        });
    </script>
    <script>
        $(document).ready(function() {
        // Transition effect for navbar 
            toggleNav();
            $(window).scroll(function() {
                // checks if window is scrolled more than 500px, adds/removes solid class
                if($(this).scrollTop() > $(window).height()/2 ) { 
                    $('.navbar').addClass('solid');
                    $('.UserGreeter').addClass('solid');
                } else {
                    $('.navbar').removeClass('solid');
                    $('.UserGreeter').removeClass('solid');
                }
            });
            $(window).resize(function(){
                toggleNav();
            }); 
        });

        function toggleNav(){
            if($(window).width() > 1546) {
                $("#logoPic").show();
                $(".shortlogoPic").hide();
            } else {
                $("#logoPic").hide();
                $(".shortlogoPic").show();
            }
        }
    </script>


</Head>

<Body>
      
    <nav id="logoPic" class="navbar navbar-default navbar-fixed-top">
        <img src="HokiepediaLogo.jpg">
        <div class="UserGreeter">
            <label>Hello, <?php session_start(); 
                                if(empty($_SESSION['name'])) {
                                    echo "User";
                                } else {
                                    echo $_SESSION['name'];
                                }  
                                ?>
            </label>
        </div>
    </nav>

    <!-- Navagation Bar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../GeorgesPages/homePage.php"><i class="glyphicon glyphicon-home"></i> <label>Home</label></a>
                <a class="navbar-brand" href="../Annie/Course_Catalog.php"><i class="glyphicon glyphicon-apple"></i><label>Class</label></a>
                <a class="navbar-brand" href="../Annie/departmentCatalog.php"><i class="glyphicon glyphicon-education"></i><label>Majors</label></a>
                <a class="navbar-brand" href="../Julie/BusinessPage.php"><i class="glyphicon glyphicon-usd"></i><label>Business</label></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../Annie/Profile_Page.php"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                    <li><a href="../Ryan/AccountSettingsPage.php"><i class="glyphicon glyphicon-cog"></i>Settings</a></li>
                    <li><a href="../GeorgesPages/homePage.php"> <i class="glyphicon glyphicon-remove-sign"></i> Logout</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    

    <!-- Class Title -->

    <div class="container">

    <div class="shortlogoPic">
        <img src="HokiepediaLogo.jpg">
        <div class="UserGreeter">
            <label>Hello, <?php session_start(); 
                                if(empty($_SESSION['name'])) {
                                    echo "User";
                                } else {
                                    echo $_SESSION['name'];
                                }  
                                ?>
            </label>
        </div>
    </div>

    <div class="headerText">
        <h1> <?php 
                echo $_GET["type"]." ".$_GET['HeaderName'];  
            ?></h1>
        <!-- MYSQL CODE TO GENERAL THE CLASS HERE -->
    </div>


        <div class="row">


            <div class="col-md-2" id="TextChangeHeader">
                <h4>Change Content: </h4>
            </div>

            <div class="col-md-3">
                    <button class="btn">Typical Syllabus</button>
                    <div class="dropdown">
                        <button class="btn" style="border-left:1px solid navy">
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-content">
                            <?php  
                                $header ="";
                                require_once("db.php");

                                if(isset($_GET['CID'])) {
                                    $Num = $_GET['CID'];

                                    $sql = "SELECT `HeaderName` 
                                            FROM `ClassContribution` 
                                            WHERE Class_ID = $Num";

                                    $result = $mydb->query($sql);

                                    while($row=mysqli_fetch_array($result)){  
                                        echo "<a href='#' name='HI' id='".$row['HeaderName']."'>".$row['HeaderName']."</a>";
                                    }

                                } else {
                                    echo "<a href='#'>Typical Syllabus</a>";
                                }
                            ?>
                            <!-- <a href="#"> HTML</a>
                            <a href="#"> CSS</a>
                            <a href="#"> JavaScript</a>
                            <a href="#"> jQuery</a> -->
                        </div>
                    </div>
            </div>
        </div> <!-- End of Header -->
        <br>

        <div class="row">
            <div class="col-md-12">
                <h3>CONTENT: 
                    <?php  
                        $header ="";
                        $id= 0;
                        require_once("db.php");

                        if(isset($_GET['HeaderName']) && $_GET['CID']) {
                            $id = $_GET['CID'];
                            $header = $_GET['HeaderName'];
                            $sql = "SELECT `HeaderName` 
                                            FROM `ClassContribution` 
                                            WHERE Class_ID = $id
                                            AND HeaderIndex = 1";
                            $result = $mydb->query($sql);

                            while($row=mysqli_fetch_array($result)){  
                                echo $row['HeaderName'];
                                }
                        } else {
                            echo "Typical Syllabus";
                        }
                        ?></h3>
            </div>
        </div>

        <!-- WYSIWYG Editor &  submit, cancel Buttons-->
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
            <textarea name="data"></textarea>


            <br>
            <br>

            <div class="row">
                <div class="col-lg-2">
                    <button type="submit" name="submit" value="submit" class="Endbutton">Create</button>
                </div>
                <div class="col-lg-2">
                    <button class="Endbutton"><a href="ClassPage.php">Cancel</a></button>
                </div>
        </div>


        </form>

    </div> <!-- End of container -->
    
    
    <div class="footer">
        <div class="row">
            <div class="col-lg-4" id="aboutfooter">
                <footer>
                    About Us
                    <br>
                    BIT 4444 Team Production
                </footer>
            </div>
            <div class="col-lg-6">
            </div>
            <div class="col-lg-2" id="aboutfooter2">
                <footer>
                    <u>Hokiepedia inc.</u>
                </footer>
            </div>
        </div>

</Body>

</Html>