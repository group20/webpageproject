<!-- Joseph's Page -->
<?php
  $username = "";
  $fname = "";
  $usertype = "";
  $major = "";
  $userID = 1;
  $BusID = 1; //needs to be dynamic later

    session_start();
    if(!empty($_Session['username'])) {
        $username = $_SESSION['username'];
        
        require_once("db.php");

        $sql = "SELECT User_ID FROM user WHERE Username = '$username'";
        $result = $mydb->query($sql);
        $row=mysqli_fetch_array($result);

        if($row){
            $userID = $row['User_ID'];
        } else {
            $userID = 1;
        }
    } else {
        $userID = 1;
    }


  $BName = "";
  $BusSel = "";
  $SelectChoice = "";
  $BusTitle= "";
  $StartDay = 1;
  $StartMon = 1;
  $StartYear = 1;
  $EndDay = 1;
  $EndMon = 1;
  $EndYear = 1;
  $BusPay =0;
  $data = "";
  $err = true;

  if (isset($_POST["submit"])){
      if(isset($_POST["BusName"]))      $BName = $_POST["BusName"];
      if(isset($_POST['BusSel']))       $BusSel = $_POST['BusSel'];
      if(isset($_POST['SelectChoice'])) $BusSel = $_POST['SelectChoice'];
      if(isset($_POST['BusTitle']))     $BusTitle = $_POST['BusTitle'];
      if(isset($_POST['StartDay']) && $_POST['StartDay']!=0)    $StartDay = $_POST['StartDay'];
      if(isset($_POST['StartMon']) && $_POST['StartMon']!=0)    $StartMon = $_POST['StartMon'];
      if(isset($_POST["StartYear"]) && $_POST['StartYear']!=0)  $StartYear = $_POST["StartYear"];
      if(isset($_POST['EndDay']) && $_POST['EndDay']!=0)        $EndDay = $_POST['EndDay'];
      if(isset($_POST['EndMon']) && $_POST['EndMon']!=0)        $EndMon = $_POST['EndMon'];
      if(isset($_POST["EndYear"]) && $_POST['EndYear']!=0)      $EndYear = $_POST["EndYear"];
      if(isset($_POST['BusPay']))       $BusPay = $_POST['BusPay'];
      if(isset($_POST['data']))         $data = $_POST['data'];
      $err = false;


  if(!$err && !empty($BName) && !empty($BusTitle) && !empty($data)){
      require_once("db.php");

      $sql = "INSERT INTO `businessuser`( `User_ID`, `Bus_ID`, `Position`, `Pay`, `StartDate`, `EndDate`, `Contribution`) 
                VALUES ($userID, $BusID, '$BusTitle', $BusPay, '$StartYear-$StartMon-$StartDay','$EndYear-$EndMon-$EndDay', '$data' )";
      $result = $mydb->query($sql);


      header("HTTP/1.1 307 Temporary Redirect");
      header("Location: ../Julie/BusinessPage.php");
  }
}

?>

<!DOCTYPE html>
<Html>
<Head>
    <Title>Edit Business Page</Title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="MyCss.css">
    <style>
       

        div.divCol1
        {
            margin: 25;
        }
        
        label.dates {
            display: block;
            font: 1rem 'Fira Sans', sans-serif;
        }

        input.dater , label.dates {
            margin: .4rem 0;
        }

        .row {
            margin-bottom: 40px;
        }

        #UserBOX {
            background-color: #EEEEEE ;
            border-style: solid;
            border-width: 0.5px;
            border-color: black;
        }

        .row.summaryinput {
            margin-left: 5px;
            margin-right: 5px;
        }

</style>
<!-- Script for the textarea -->
    <script src="https://cdn.tiny.cloud/1/4of2n961lvuhmwbg31ghm8k2dzl1949uwvq773gpa2an43x1/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({ 
            selector:'textarea', 
            plugins: 'link',
            menubar: false,
            toolbar: 'undo redo | selectall copy paste cut | bold italic underline | alignleft aligncenter alignright alignnone | link '
        });
    </script>
<!-- Ending Script for textarea -->


    <script>

        $(document).ready(function() {
        // Transition effect for navbar 
            toggleNav();
            $(window).scroll(function() {
                // checks if window is scrolled more than 500px, adds/removes solid class
                if($(this).scrollTop() > $(window).height()/2 ) { 
                    $('.navbar').addClass('solid');
                    $('.UserGreeter').addClass('solid');
                } else {
                    $('.navbar').removeClass('solid');
                    $('.UserGreeter').removeClass('solid');
                }
            });
            $(window).resize(function(){
                toggleNav();
            }); 
        });

        function toggleNav(){
            if($(window).width() > 1546) {
                $("#logoPic").show();
                $(".shortlogoPic").hide();
            } else {
                $("#logoPic").hide();
                $(".shortlogoPic").show();
            }
        }
    </script>

     <script>
      var asyncRequest;

      function getContent() {
        var choice = document.forms[0].BusSel.value;
        var z = document.getElementById("DropDownSelect");
        if(choice=="") {
          z.innerHTML = "";
        } else {
          try {
  					asyncRequest = new XMLHttpRequest();  //create request object

  					//register event handler
  					asyncRequest.onreadystatechange=stateChange;
                    var url="getMajorClass.php?choice="+choice;
  					asyncRequest.open('GET',url,true);  // prepare the request
  					asyncRequest.send(null);  // send the request
  				}
  					catch (exception) {alert("Request failed");}

        }

        function stateChange() {
          // if request completed successfully
          if(asyncRequest.readyState==4 && asyncRequest.status==200) {
            document.getElementById("DropDownSelect").innerHTML=
              asyncRequest.responseText;  // places text in contentArea
          }
        }
      }
    </script>

    <script>
         function init() {
			var x = document.getElementsByTagName("form")[0];
		//	x.addEventListener("submit", checkInputs);
        //    x.addEventListener("change", checkGrad);

        }
        document.addEventListener("DOMContentLoaded", init);
    </script>

</Head>
<Body>
 
<!-- Leftside Logo -->
<nav id="logoPic" class="navbar navbar-default navbar-fixed-top">
    <img src="HokiepediaLogo.jpg">
    <div class="UserGreeter">
        <label>Hello, <?php session_start(); 
                            if(empty($_SESSION['name'])) {
                                echo "User";
                            } else {
                                echo $_SESSION['name'];
                            }  
                            ?>
        </label>
    </div>
</nav>

<!-- Navagation Bar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../GeorgesPages/homePage.php"><i class="glyphicon glyphicon-home"></i> <label>Home</label></a>
            <a class="navbar-brand" href="../Annie/Course_Catalog.php"><i class="glyphicon glyphicon-apple"></i><label>Class</label></a>
            <a class="navbar-brand" href="../Annie/departmentCatalog.php"><i class="glyphicon glyphicon-education"></i><label>Majors</label></a>
            <a class="navbar-brand" href="../Julie/BusinessPage.php"><i class="glyphicon glyphicon-usd"></i><label>Business</label></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../Annie/Profile_Page.php"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                <li><a href="../Ryan/AccountSettingsPage.php"><i class="glyphicon glyphicon-cog"></i>Settings</a></li>
                <li><a href="../GeorgesPages/homePage.php"> <i class="glyphicon glyphicon-remove-sign"></i> Logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>


<!-- Container for box for user input/tabs etc... -->
<div class="container" id="content">
    <!-- Center Logo when resized -->
    <div class="shortlogoPic">
        <img src="HokiepediaLogo.jpg">
        <div class="UserGreeter">
            <label>Hello, <?php session_start(); 
                                if(empty($_SESSION['name'])) {
                                    echo "User";
                                } else {
                                    echo $_SESSION['name'];
                                }  
                                ?>
            </label>
        </div>
    </div>

    <div class="headerText">
        <h1>Add Business</h1>
 
    </div>
    
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >

    <!-- Form within the tabs -->
        <div class="container-fluid" id="UserBOX"> 
        <br/>
        <!-- Row ONE -->
            <div class="row">
                <div class="col-md-2">
                    <div class="divCol1">
                        <label for="#BusName">BUSINESS NAME</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <input name="BusName" type="text" size="35"/> 
                </div>

                <div class="col-md-2">
                    <label>For a Class or Major?</label>
                    <div class="form-group">
                        <label for="ClassRadio" class="radio-inline">
                            <input type="radio" name="BusSel" value="Class" id="ClassRadio" onchange="getContent()">
                            Class
                        </label>
                        <label for="MajorRadio" class="radio-inline">
                            <input type="radio" name="BusSel" value="Major" id="MajorRadio" onchange="getContent()">
                            Major
                        </label>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div id="DropDownSelect">
                       
                    <!-- AJAX -->
                   
                    </div>
                </div>
            </div>
            

            <!-- Row TWO -->
            <div class="row">
                <div class="col-md-2">
                    <div class="divCol1">
                        <label>Title</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <input name="BusTitle" type="text" size="35"/> 
                </div>
                <div class="col-md-6">
                    <input type="checkbox"/>
                    <label > Currently Employeed</label>
                </div>
            </div>
   

            <!-- Row THREE -->
            <div class="row"> 
                <Div class="col-md-2">
                    <label>Start Date</label>
                </Div>
                <div class="col-md-1">
                    <div class="Drop-group">
                        <select name="StartDay" id="SDay">
                            <option value="0">Day</option>
                            <?php
                                    $i = 1;
                                    while($i != 32){
                                        echo "<option value='$i' >$i</option>";   
                                        $i += 1;
                                        echo"<br>";
                                    }
                                ?>
                         </select>
                        
                    </div>
                </div>                        
                <div class="col-md-1">
                    <div class="Drop-group">
                        <select name="StartMon" id="SMon">
                            <option value="0">Month</option>
                            <option value="01">Jan</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Apr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Aug</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="Drop-group">
                        <select name="StartYear" id="SYear">
                            <option value="0">Year</option>
                            <?php
                                $i = 2000;
                                while($i != 2019){
                                    echo "<option value='$i'>$i</option>";
                                    $i +=1;
                                }
                            ?>
                        
                        </select>
                    </div>
                </div>


                <Div class="col-md-2">
                    <label>End Date</label>
                </Div>
                <div class="col-md-1">
                    <div class="Drop-group">
                        <select name="EndDay" id="SDay">
                            <option value="0">Day</option>
                            <?php
                                    $i = 1;
                                    while($i != 32){
                                        echo "<option value='$i' >$i</option>";   
                                        $i += 1;
                                        echo"<br>";
                                    }
                                ?>
                         </select>
                        
                    </div>
                </div>                        
                <div class="col-md-1">
                    <div class="Drop-group">
                        <select name="EndMon" id="SMon">
                            <option value="0">Month</option>
                            <option value="01">Jan</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Apr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Aug</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="Drop-group">
                        <select name="EndYear" id="SYear">
                            <option value="0">Year</option>
                            <?php
                                $i = 2000;
                                while($i != 2019){
                                    echo "<option value='$i'>$i</option>";
                                    $i +=1;
                                }
                            ?>
                        
                        </select>
                    </div>
                </div>
            </div>
    

            <!-- Row FOUR -->
            <div class="row"> 
                <div class="col-md-2">
                    <label>Pay</label>
                </div>
                <div class="col-md-4" >
                    <input name="BusPay" type="text" size="35px">
                    <div class="form-group">
                        <!-- Has not place in the database -->
                        <label for="MonthPayRadio" class="radio-inline">
                            <input type="radio" name="PaySel" value="Monthly" id="MonthPayRadio" onchange="getContent()">
                            Monthly
                        </label>
                        <label for="YearPayRadio" class="radio-inline">
                            <input type="radio" name="PaySel" value="Yearly" id="YearPayRadio" onchange="getContent()">
                            Yearly
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <br>
                    <!-- Has not place in the database -->
                    <div class="ui-grid-b ">
                        <div class="ui-block-a">
                            <input type="radio" value="Internship" name="PosType" id="Radio-Employment-Type"/>
                            <label for="Radio-Monthly">Internship</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                <br>
                    <div class="ui-grid-b ">
                        <div class="ui-block-a">
                            <input type="radio" value="Externship" name="PosType" id="Radio-Employment-Type"/>
                            <label for="Radio-Monthly">Externship</label>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2">
                <br>
                    <div class="ui-grid-b ">
                        <div class="ui-block-a">
                            <input type="radio" value="Employee" name="PosType" id="Radio-Employment-Type"/>
                            <label for="Radio-Monthly">Employee</label>
                        </div>
                    </div>
                </div>
            </div>      

          

            <!-- Row Five -->
            <div class="row summaryinput">
                <h3>Summary<small>of your position</small></h3>

                <!-- Textarea for contributions -->
                <textarea name="data"></textarea>
            </div>

        </div>
        
        <br>
        <br>
        <div class="row">
            <div class="col-lg-2">
                <button type="submit" name="submit" value="submit" class="Endbutton">Create</button>
            </div>
            <div class="col-lg-2">
                <button class="Endbutton"><a href="../GeorgesPages/homePage.php">Cancel</a></button>
            </div>
          
        </div>

    </form>
</div>


    <div class="footer">
        <div class="row">
            <div class="col-lg-4" id="aboutfooter">
                <footer>
                    About Us
                    <br>
                    BIT 4444 Team Production
                </footer>
            </div>
            <div class="col-lg-6">
            </div>
            <div class="col-lg-2" id="aboutfooter2">
                <footer>
                    <u>Hokiepedia inc.</u>
                </footer>
            </div>
        </div>

</Body>
</Html>