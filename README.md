# WebpageProject

Outline

Terms/NOTES:
        Contribution: When people contribute info like a wikipedia page 
                --> Contributions will be on the... 
                        - Class Page, 
                        - Business Page, 
                        - Major Page
        
        Comments: People commenting is usully a discussion type text. Like reddit or our class discussions
                --> comments will be on.. 
                        - ALL forum pages. 

        Profile Vs Account: 
                --> Profile is what others can see, Like your page on Facebook. 
                        - Should be able to see comments that that user as contributed. 
                --> Account is the settings. Password change, username change, preferences, etc...
                --> Tip: for Profile, look at a user page on Reddit, you can see what they commented on. 
        
        Catalog pages: Basically just a list of whatever we heave
                --> Course Catalog: should be a nice list of course 
                --> Department Catalog: list of departments (Like Pamplin consist of BIT - DSS, BIT -OSM, ACIS, FIN, etc.) 
                                        that you can click on the majors under the department and a list of courses for that 
                                        major should populate.   
        
        Connecting Pages:
                --> general nav buttons, use the page link to go to it. 
                    - Look at the Useful Document in googleDocs for help
                --> IMPORTANT: pass id in the Parameters of a URL (id part of the page) when redirecting. 
                        - www.hokiepedia.com/Edit_Business_Page.php?id=002&BusName=Deloitte
                            - ex. above shows that you are being redirected to Edit Business Page with the Users ID (002) and   the Business that will be editted (Deloitte)
                    -How to Pass URL info:
                        - Start with a ?
                        - name it whatever you want, as long as the page it is directing to knows what you named it.
                        - use & to add more parameters.
                --> EVERY PAGE you Create will have to be able to read session and URL parameters so you pull the correct               information from the database. 

**Pages Assigned**

    Joseph M Spence
        Class Page                  : The class Conbtribution Page
        Edit Business Page          : This page edits the business contribution Main page
        General Edit page           : This page edits the class contribution main page  
        Account Creation page       : Creation of a NEW account. 

    George Scharps
        Home page
        Add Page
        Report Site Issue Page
        Login Page

    Julie Crowe
        Class Forum Page
        Major Forum Page
        Business Page
        Business Forum

    Ryan
        Major Page
        Account Settings Page
        Admin Page/Dashboard
        Admin Login

    Annie
        Profile Page
        Edit Profile Page
        Course Catalog Page
        Department Catalog



  **Directories**

    \WebpageProject\Annie
        host:   Course_Catalog.php, departmentCatalog.php, Profile_Edit_Page.php, Profile_Page.php

    \WebpageProject\GeorgesPages
        host:   addPage.php, homePage.php, loginPage.php, reportIssue.php

    \WebpageProject\Joseph
        host:   Account_Creation_Page.php, ClassPage.php, Edit_Business_Page.php, General_Edit_Page.php

    \WebpageProject\Julie
        host:   BusinessForum.php, ClassForum.php, displayComments.php, MajorForum.php

    \WebpageProject\Ryan
        host:   AccountSettingsPage.php, AdminDashboard.php, AdminLogin.php, MajorPage.php

    \WebpageProject\Template
            **Houses general templates for the whole site. CSS files, JavaScript File, etc...
