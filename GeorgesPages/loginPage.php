<?php 
$username = "";
$password = "";
$error = "";
$loginOK = null;
$usertype = null;
$errorMessage = null;

if(isset($_POST["submit"])){
    if(isset($_POST["username"])) $username=$_POST["username"];
    if(isset($_POST["password"])) $password=$_POST["password"];

    if(empty($username) || empty($password)){
        $error=true;
    }

    if(!$error){
        require_once("db.php");
        $sql = "SELECT * from user where Username='$username'";
        $result = $mydb->query($sql);

        $row=mysqli_fetch_array($result);
        if($row){
            if(strcmp($password, $row["PWD"])==0){
                $loginOK=true;
            }else{
                $loginOK = false;
                $errorMessage = "Incorrect Username and/or Password";
            }
        }
        if($loginOK){
            //set session variable to remember the various info
            session_start();
            $_SESSION["name"] = $row["firstName"];
            $_SESSION["userType"] = $row["UserType_ID"];
            $_SESSION["Major"] = $row["Major"];
            $_SESSION["username"] = $row["Username"];
            
            Header("Location:homePage.php");
        }
    }

}

?>
<!doctype html>
<html>
    <head>
        <title>Hokiepedia - Login</title>
        <script>//insert js code about appending error messages</script>
        <style>
        body{
            background-color:maroon;
            margin: 0;
            padding: 0;
        }

        form{
            background-color:#ff751a;
            border-style:solid;
            width: 300px;
            box-shadow: 3px 3px black;
            margin: 0 auto;
            margin-top: 50px;
            padding-bottom: 20px;
        }
        #picture{
            margin-top: 50px;
        }
    
        </style>
        <script src="jquery-3.1.1.min.js"></script>
        <script>
        function checkInputs(event){
            var frmObject = document.forms[0];
            var output = "Username and/or Password is not valid.";
            var checkError = false;

            if (frmObject.username.value==""){
                checkError = true;
                
            }
            if(frmObject.password.value==""){
                checkError = true;
            }

            if(checkError){
                alert(output);
                event.preventDefault();
                return;
            }
        }
        function init(){
            var x = document.getElementsByTagName("form")[0];
            x.addEventListener("submit", checkInputs);
        }
        document.addEventListener("DOMContentLoaded", init);
        </script> 
    </head>
    <body>
        <div id="picture" align="center">
        <img align="middle" src="vtlogo.png">
        </div>
        <form id="form_login" method="post" align="center">

            <label><font color="black"><h1>Username:</h1></font></label>
            <input id="username" type="text" name="username"value="<?php if(!empty($username)) echo $username;?>">
            <label><font color="black"><h1>Password:</h1></font></label><br>
            <input type="password" name="password" value=""><br><br>
            <?php echo "<p class='error'>".$errorMessage."</p>";?>
            <a href="homePage.php">
            <input id="cancel" type="button" name="cancel" value="Cancel">
        </a>
            <input type="submit" name="submit" value="Submit">
            
            <a href="reportIssue.php"><input type="button" name="rIssue" value="Report Issue"></a><br><br>
            <a href="../Joseph/Account_Creation_Page.php"><font color="black">Create New Account</font></a>
            
        </form>
        
    </body>
</html>





