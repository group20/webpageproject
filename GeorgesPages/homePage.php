
<!doctype html>
<html id=home>

<head>
  <title>HokiePedia - Home</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body{
      background-color:maroon;
    }
    h1{
      text-align:center;
    }
    
  </style>

</head>

<body>

  <div class="container-fluid" align="center">
    <ul align="center" class="nav nav-pills">
      
      <?php 
      session_start();
      $userName = "";
      $name = "";
      if(isset($_SESSION["username"])) $userName = $_SESSION["username"];
      if(isset($_SESSION["name"])) $name = $_SESSION["name"];
         echo "<li class='active'><a href='homePage.php'>Home</a></li>";
         
          if (!empty($userName)) echo "<li><a href='../Julie/BusinessPage.php'>Business</a></li>"; 
          if (!empty($userName)) echo "<li><a href='../Annie/departmentCatalog.php'>Department Catalog</a></li>";
          if (!empty($userName)) echo "<li><a href='../Annie/Course_Catalog.php'>Courses</a></li>";
          if (!empty($userName)) echo "<li><a href='addPage.php'>Add Page</a></li>";
          if (!empty($userName)) echo "<li><a href='../Annie/Profile_Page.php'>My Profile</a></li>";
          if (!empty($userName)) echo "<li><a href='../Ryan/AccountSettingsPage.php'>Account Settings</a></li>"; 
          if (empty($userName)) echo "<li><a href='loginPage.php'>Login</a></li>";
          
          if (!empty($name)) echo "<p><font color='black'>Hello, ".$name."!</font></p>";
          if (empty($userName)) echo "<li><a href='../Joseph/Account_Creation_Page.php'>Create An Account</a></li>";
       ?>
    </ul>

  </div>
  
  <br><br>
  <table align="center">
<tr>
<td>
<img src="vtlogo.png"></td>
</tr>
<tr>
<td>
<h1><font color="black">Welcome To HokiePedia!</font></h1></td>
</tr>
</table>
<p align="center"><font color="black">
HokiePedia is a place where students and graduates of Virginia Tech<br>
come together to share information about the various courses and <br>
majors offered. Rather than having to rely on a handful of people<br>
to obtain information on all the different courses and majors <br>
offered at Virginia Tech, HokiePedia is a central location for many <br>
people to contribute useful information so that you can<br> have a more well-rounded idea of what to expect.
</font>
</p>
<br><br><br><br><br><br><br><br>
<div align="center"><a href="reportIssue.php"><button>Report Site Issue</button></a>
</div>

</body>

</html>
