<?php
    require_once("db.php");

    $userid = 0;
    $datecreated = 2; //date('Y/m/d');
    $comment = "";
    $year = 0;
    $semester = "";
    $err = false;
    $class = "";


    if (isset($_POST["submit"])) {
        if(isset($_POST["User_ID"])) $userid=$_POST["User_ID"];
        if(isset($_POST["Created"])) $datecreated=$_POST["Created"];
        if(isset($_POST["data"])) $comment=$_POST["data"];
        if(isset($_POST["SemesterYear"])) $year=$_POST["SemesterYear"];
        if(isset($_POST["Semester"])) $semester=$_POST["Semester"];

        // if(isset($_POST['data'])) { 
        //     $TextArea=$_POST['data'];

    
        if(!empty($userid) && !empty($datecreated) && !empty($comment)) {
          header("HTTP/1.1 307 Temprary Redirect");
          header("Location: displayClassComments.php");
        } else {
          $err = true;
        }
    }

    //link forum to major page via Major_ID

    if(isset($_GET['class'])) {
        $Bus_ID=$_GET['class'];

        $sql="SELECT TabHeader
        FROM ClassForum as p, classes as m
        WHERE p.Class_ID = m.Class_ID
        AND m.ClassName = '$class'";

        $result = $mydb->query($sql);

        if($row=mysqli_fetch_array($result)){
        echo "Your class name is ".$row['TabHeader'];
        } else {
        echo "Your class name cannot be found.";
        }
    }
    else{
        //default page
    }
  ?>

<!DOCTYPE html>
<html>
<head>
<title>Class Forum Page</title>

<style>
    body {background-color: maroon;}
    .button {
        background-color: orange;
        font-family: arial;
        }

    th, td {
    padding: 3px;
    vertical-align:top;
    }

    #CommentSection {
        word-wrap: break-word;
        width: 500px;
    }

    #count{
        text-align: center;
    }
</style>

<script src="jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--textbox w bootstrap-->
<script src="https://cdn.tiny.cloud/1/4of2n961lvuhmwbg31ghm8k2dzl1949uwvq773gpa2an43x1/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({ 
            selector:'textarea', 
            plugins: 'link',
            menubar: false,
            toolbar: 'undo redo | selectall copy paste cut | bold italic underline | alignleft aligncenter alignright alignnone | link '
        });

    </script>

</head>

<body>
    <div style="background-color:white;
        font-family: arial;
        width:65%">

    <h1 id=header>Class Forum</h1>

    <!-- <div id="contentArea">&nbsp;</div><br> -->


    <?php
        echo "<table border='1' class='comments' style='background-color:white; font-family:arial;'>
        <tr>
            <th>User ID</th>
            <th>Date</th>
            <th>Comment</th>
        </tr>";

        require_once("db.php");

        $id = 0;
        if(isset($_GET['id'])) $id = $_GET['id'];

            if($id==0){
            $sql = "SELECT * FROM ClassComment";
            $result = $mydb->query($sql);
            while ($rows=mysqli_fetch_array($result))
            echo "<tr><td id='count'> ". $rows['User_ID']. "</td><td>". $rows['Created']. "</td><td id='CommentSection'> ". $rows['Comment'] .
                "</td><td>". "<button type='button' style='background-color:orange; font-family:arial; font-size: 12pt;'>Modify</button>". "</td></tr>";
            }

        echo "</table>"
            ?>


    <h3>Post a comment:</h4>
    
    <label>Choose a Company:</label>
        <select name="table" id = "class">
            <option value=""></option>

        <?php
        //list all majors in db
        $result = $mydb->query("SELECT Distinct ClassName FROM classes");
        
        while ($rows=mysqli_fetch_array($result)){
            $i = $rows['ClassName'];
            echo "<option value='$i'>$i</option>";
        }
        ?>
        </select>
        <br>
        
        User ID: <input type="text" class=userID> <br>
        Date: <input type="date" class=date> <br>
        <br>
        <!-- textbox code-->
        <textarea name="data" style="width:60%"></textarea>
        </div>

    <input type="submit" name="submit" value="Submit"/>
    <a href="../Joseph/ClassPage.php"><input type="submit" name="cancel" value="Cancel"></a>
</body>

</html>