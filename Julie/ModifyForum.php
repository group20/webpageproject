<?php
    require_once("db.php");

    $userid = 0;
    $datecreated = 2; //date('Y/m/d');
    $comment = "";
    $year = 0;
    $semester = "";
    $err = false;
    $major = "";

    if (isset($_POST["submit"])) {
        if(isset($_POST["User_ID"])) $userid=$_POST["User_ID"];
        if(isset($_POST["Created"])) $datecreated=$_POST["Created"];
        if(isset($_POST["data"])) $comment=$_POST["data"];
        if(isset($_POST["SemesterYear"])) $year=$_POST["SemesterYear"];
        if(isset($_POST["Semester"])) $semester=$_POST["Semester"];
    }

    if(isset($_GET['major'])) {
        $Major_ID=$_GET['major'];

        $sql="SELECT TabHeader
        FROM MajorForum as p, majors as m
        WHERE p.Major_ID = m.Major_ID
        AND m.MajorName = '$major'";

        $result = $mydb->query($sql);

        if($row=mysqli_fetch_array($result)){
        echo "Your your name is ".$row['TabHeader'];
        } else {
        echo "Your major name cannot be found.";
        }
    }
    else{
        //default page
    }
?>

<!DOCTYPE html>
<html>
<head>
<title>Modify Forum</title>
<style>
    body {background-color: maroon;}
    .button {
        background-color: orange;
        font-family: arial;
        }
</style>

<script src="jquery-3.1.1.min.js"></script>

<script>
    var asyncRequest;

function getContent() {
    var url = "displayComments.php";
        try {
            asyncRequest = new XMLHttpRequest();

            asyncRequest.onreadystatechange=stateChange;
            asyncRequest.open('GET',url,true);
            asyncRequest.send(null);
        }
            catch (exception) {alert("Request failed");}
        }

function stateChange() {
if(asyncRequest.readyState==4 && asyncRequest.status==200) {
    document.getElementById("contentArea").innerHTML=asyncRequest.responseText;
    console.log("end state change");
    }
}

function init(){
    document.getElementById("major").addEventListener("load", getContent(), false);
}

document.addEventListener("DOMContentLoaded", init);

</script>

</head>
<body>
    <div style="background-color:white;
        font-family: arial;
        width:75%">

    <h1 id=header>Major Forum</h1>

    <div id="contentArea">&nbsp;</div>
</body>