<?php
    require_once("db.php");

    $userid = 0;
    $datecreated = date('Y/m/d');
    $comment = "";
    $year = 0;
    $semester = "";
    $err = false;
    $business = "";


    if (isset($_POST["submit"])) {
        if(isset($_POST["User_ID"])) $userid=$_POST["User_ID"];
        if(isset($_POST["Created"])) $datecreated=$_POST["Created"];
        if(isset($_POST["data"])) $comment=$_POST["data"];
        if(isset($_POST["SemesterYear"])) $year=$_POST["SemesterYear"];
        if(isset($_POST["Semester"])) $semester=$_POST["Semester"];

        // if(isset($_POST['data'])) { 
        //     $TextArea=$_POST['data'];

    
        if(!empty($userid) && !empty($datecreated) && !empty($comment)){
            $sql = "INSERT INTO `BusinessComments`( `BF_ID`, `User_ID`, `Created`, `Comment`, `Likes`, `Dislikes`) VALUES (1, $userid, '$datecreated','$comment', 0, 0 )";
            $result = $mydb->query($sql); {
                }
        }
    }

    //link forum to major page via Major_ID

    if(isset($_GET['business'])) {
        $Bus_ID=$_GET['business'];

        $sql="SELECT TabHeader
        FROM BusinessForum as p, business as m
        WHERE p.Bus_ID = m.Bus_ID
        AND m.BusName = '$business'";

        $result = $mydb->query($sql);

        if($row=mysqli_fetch_array($result)){
        echo "Your company name is ".$row['TabHeader'];
        } else {
        echo "Your company name cannot be found.";
        }
    }
    else{
        //default page
    }
  ?>

<!DOCTYPE html>
<html>
<head>
<title>Business Forum Page</title>

<style>
    body {background-color: maroon;}
    .button {
        background-color: orange;
        font-family: arial;
        }

    th, td {
    padding: 3px;
    vertical-align:top;
    }

    #CommentSection {
        word-wrap: break-word;
        width: 500px;
    }

    #count{
        text-align: center;
    }
</style>

<script src="jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--textbox w bootstrap-->
<script src="https://cdn.tiny.cloud/1/4of2n961lvuhmwbg31ghm8k2dzl1949uwvq773gpa2an43x1/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({ 
            selector:'textarea', 
            plugins: 'link',
            menubar: false,
            toolbar: 'undo redo | selectall copy paste cut | bold italic underline | alignleft aligncenter alignright alignnone | link '
        });    

        //show comments db when page is loaded
        

    </script>

</head>

<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <div style="background-color:white;
        font-family: arial;
        width:70%">

    <h1 id=header>Company Forum</h1>


    <?php
    echo "<table border='1' class='comments' style='background-color:white; font-family:arial;'>
    <tr>
        <th>Company</>
        <th>User ID</th>
        <th>Date</th>
        <th>Comment</th>
    </tr>";

    require_once("db.php");

    $id = 0;
    if(isset($_GET['id'])) $id = $_GET['id'];

        if($id==0){
        $sql = "SELECT * FROM BusinessComments";
        $result = $mydb->query($sql);
        while ($rows=mysqli_fetch_array($result))
        echo "<tr><td id='count'> ". $rows['User_ID']. "</td><td>". $rows['Created']. "</td><td id='CommentSection'> ". $rows['Comment'] . "</td></tr>";
        }

    echo "</table>"
    ?>

    <h3>Post a comment:</h4>
    
    <label>Choose a Company:</label>
        <select name="table" id = "business">
            <option value=""></option>

        <?php
        //list all majors in db
        $result = $mydb->query("SELECT Distinct BusName FROM business");
        
        while ($rows=mysqli_fetch_array($result)){
            $i = $rows['BusName'];
            echo "<option value='$i'>$i</option>";
        }
        ?>
        </select>
        <br>
        
        User ID: <input type="text" class=userID name='User_ID'> <br>
        Date: <input type="date" class=date name='Created'> <br>
        <br>
        <!-- textbox code-->
        <textarea name="data" style="width:60%" name='Comment'></textarea>
        </div>

    <input type="submit" name="submit" value="Submit"/>
    <a href="../Julie/BusinessPage.php"><input type="submit" name="cancel" value="Cancel"></a>
    </form>
</body>

</html>