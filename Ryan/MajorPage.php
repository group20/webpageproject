<!DOCTYPE html>
<Html>
<head>
  <meta charset="utf-8">
  <title>Welcome to the Major Page!</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Javascript used for an image on Admin Dashboard with mouseover and mouseout -->
    <script>
      function init(){
        document.images[0].addEventListener("mouseover", imageOver());
        document.images[0].addEventListener("mouseout", imageOut());
        console.log("inital");
      }

      function imageOver(){
        document.images[0].src="vt.jpeg";
        console.log("imageOver");
      }

      function imageOut(){
        document.images[0].src="popularmajors.jpeg";
        console.log("imageOut");

      }

      document.addEventListener("DOMContentLoaded", init);
    </script>

  <style type="text/css">
      body	{
          background-color: lightblue;
          box-shadow: shadow;
      }

      .maroon	{
          color: green;
          font-family: Arial;
          font-variant: small-caps;
          font-weight: 800;
          font-size: 20pt;
      }

      .tbl	{
          margin-left: .35in;
          margin-top: .35in;
        }

        #srcm	{
          font-weight: 850;
          color: blue;
        }

        div p	{
          margin-left: 500px;
          margin-right: 750px;
          margin-top: 2em;
          margin-bottom: 3em;
          background-color: grey;
        }
        img {
          width: 400px;
        }
  </style>

  </head>

<body>
   <img src="vt.jpeg" alt="">
  <header>
    <h1>Virginia Tech Majors</h1>
  </header>

  <form class="search form" action="MajorPage.php">
    <input type="text" placeholder="Search Majors" name="search">
    <button type="submit"><i class="fa fa-search"></i></button>
  </form>

  <pre>
    Welcome to the Virginia Tech Majors Page! Students with a Hokiepedia account are encouraged to click on "Search Majors"
    above and look up any major offered at Virginia Tech to find out more detailed information about the major.
    Graduate Students with a Hokiepedia account are able to provide information about a major offered at Virginia Tech that they have completed.
    Your input will help out undergraduate students who search the major to find more detailed information. Thank you!
  </pre>

<a href="../GeorgesPages/reportIssue.php"><input type="button" value="Report Misconduct"name="Report Misconduct"></a>
<a href="../GeorgesPages/reportIssue.php"><input type="button" value = "Report General Site Issues" name="Report General Site Issues"></a>
<a href="AdminDashboard.php"><input type="button" value="Manage Page (Admin Restricted)" name="Manage Page"></a>
<button type="button"><a href="../Julie/MajorForum.php?majorName=">Write In forum</a></button>

</body>
</html>

<?php
      session_start();
      $Major = "";
      $name = "";
      if(isset($_SESSION["Major"])) $userName = $_SESSION["Major"];
      if(isset($_SESSION["name"])) $name = $_SESSION["name"];
         echo "<li class='active'><a href='../GeorgesPages/homePage.php'>Home</a></li>";
          echo "<li><a href='../Julie/BusinessPage.php'>Business</a></li>";
          echo "<li><a href='../Annie/departmentCatalog.php'>Department Catalog</a></li>";
          echo "<li><a href='../Annie/Course_Catalog.php'>Courses</a></li>";
          echo "<li><a href='AccountSettingsPage.php'>Account Settings</a></li>";
          if (empty($Major)) echo "<li><a href='../GeorgesPages/loginPage.php'>Login</a></li>";
          if (!empty($name)) echo "<p><font color='black'>Hello, ".$name."!</font></p>";
?>

<?php
  require_once ("db.php");
  $sql = "SELECT Major_ID, MajorName, OptionsAcronym FROM majors";
  $result = $mydb->query($sql);
  echo "<p>Major Forums</p>";
  echo "<ul>";
  while($row = mysqli_fetch_array($result)) {
    echo "
    <li><a href='../Julie/MajorForum.php?id=".$row['Major_ID']."'>".$row['MajorName']."- ".$row['OptionsAcronym']."</a>
    </li>";
  }
  echo "<ul>";
?>
